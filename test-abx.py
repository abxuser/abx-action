import requests
import json

def handler(context, inputs):
    print('Action started.')

    tags = inputs['tags']
    vmName = inputs["resourceNames"][0]

    tag = 'deployment-vmname'

    if 'VM' in vmName:
        tag = 'tango-tag'

    tags[tag] = 'tango-machine'

    print('Tag VM action! tag: "{}".'.format(tag))

    slackMsg = ':label: Tag VM action! tag: "' + tag + '".'
    body = {
        "channel": "#rado-demo",
        "username": "ABX",
        "text": slackMsg,
        "icon_emoji": ":bell:"
    }
    requests.post('https://hooks.slack.com/services/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', data=json.dumps(body), verify=False)

    print('Action finished!')
    return { 'tags' : tags }