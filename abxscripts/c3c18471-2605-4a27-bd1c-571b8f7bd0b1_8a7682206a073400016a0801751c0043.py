def handler(context, inputs):
    """Assign tags to a machine

    :param inputs
    :param inputs.tags: The initial tags for the machine, supplied from the event data
           during actual provisioning or from user input for testing purposes.
    :param inputs.newTags: The tags to assign to the machine, which can be
           in the form {"name": "value"} for name=value tags
           or {"name": ""} for a simple name tag.
    :return The desired tags.
    """
    new_tags = inputs["newTags"]
    outputs = {}
    outputs["tags"] = inputs["tags"]

    for key, value in new_tags.items():
        outputs["tags"][key] = value

    print("Setting tags: {0}".format(new_tags))

    return outputs
